﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace howto_load_treeview_from_xml
{
    public static class ExtensionMethods
    {
        public static String toBinary(this String str)
        {
            return Convert.ToString(Convert.ToInt32(str, 10), 2);
        }
        public static String toEightBit(this String str)
        {
            while (str.Length < 8)
                str = "0" + str;
            return str;
        }
        public static String reverseString(this String str)
        {
            String reverse = "";
            int Length = str.Length - 1;
            while (Length >= 0)
            {
                reverse = reverse + str[Length];
                Length--;
            }
            return reverse;
        }

        public static String numbtoAlpha(this String str, String text)
        {
            for (int i = 0; i < str.Length; ++i)
                if(str[i] == '1')
                    str = str.Substring(0, i) + text.Last() + str.Substring(i + 1);
            return str;
        }

        public static String maskHandler(this String str, String text)
        {
            str = str.toBinary();
            str = str.toEightBit();
            str = str.reverseString();
            str = str.numbtoAlpha(text);
            return str;
        }

        public static String opCodeCalc(TreeView treeView)
        {
            String str = "00000000";
            foreach (TreeNode item in treeView.SelectedNode.Nodes)
            {
                if (item.Text.StartsWith("PRESET_MASK")) {
                    String mask = item.Nodes[0].Text.maskHandler(item.Text);
                    for (int i = 0; i < mask.Length; ++i)
                        if(mask[i] != '0')
                        str = str.Substring(0, i) + mask[i] + str.Substring(i + 1);
                }
            }
            return str;
        }
    }
}
