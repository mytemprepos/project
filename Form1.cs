﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace howto_load_treeview_from_xml
{
    //008
    //100
    //0000 0100
    //0010 0000
    //--A- ----
    //toBinary
    //toEightBit
    //reverse
    //nubmtoAlphabe

    public partial class Form1 : Form
    {
        void Menu_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            var menuText = menuItem.Text;

            switch (menuText)
            {
                case "SHOW SCREEN":
                    String picName = null;
                    if (trvItems.SelectedNode.Text == "SCREEN_NUMBER")
                    {
                        picName = trvItems.SelectedNode.Nodes[0].Text;
                        picName.Remove(0);
                        if (ifExist(ref picName))
                            new Thread(
                            o =>
                            {
                                Invoke(new Action(() =>
                                {
                                    Funcs.showBitMap((Bitmap)Bitmap.FromFile("SCREENS\\" + picName));
                                }));
                            }).Start();
                        else
                            System.Windows.Forms.MessageBox.Show("This screen number does not exists.");
                    }
                    break;

                case "EDIT":
                    new Thread(
                        o =>
                        {
                            Invoke(new Action(() =>
                            {
                                OptionForm();
                            }));
                        }
                    ).Start();

                    break;
                case "OP CODE":
                    MessageBox.Show(ExtensionMethods.opCodeCalc(trvItems));

                    break;
            }
        }
        private void contextMenuStripInit()
        {
            ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem("SHOW SCREEN");
            toolStripMenuItem.Name = "SHOW SCREEN";
            toolStripMenuItem.Click += new EventHandler(Menu_Click);

            contextMenuStrip1.Items.Add(toolStripMenuItem);


            toolStripMenuItem = new ToolStripMenuItem("EDIT");
            toolStripMenuItem.Name = "EDIT";
            toolStripMenuItem.Click += new EventHandler(Menu_Click);

            contextMenuStrip1.Items.Add(toolStripMenuItem);

            toolStripMenuItem = new ToolStripMenuItem("OP CODE");
            toolStripMenuItem.Name = "OP CODE";
            toolStripMenuItem.Click += new EventHandler(Menu_Click);

            contextMenuStrip1.Items.Add(toolStripMenuItem);
        }
        public Form1()
        {
            InitializeComponent();
            contextMenuStripInit();
        }

        private void Iteration(XmlNodeList xmlNodeList)
        {
            MyClassBuilder MCB = null;
            Dictionary<String, Type> properties = new Dictionary<String, Type>();
            foreach (XmlNode child_node in xmlNodeList)
            {
                MCB = new MyClassBuilder(child_node.Name);
                properties.Clear();
                foreach (XmlNode grandchild_node in child_node.ChildNodes)
                    properties.Add(grandchild_node.Name, Type.GetType(grandchild_node.Attributes["type"].Value));
                Utility.Types.Add(MCB.CreateType(properties.Keys.ToList(), properties.Values.ToList()));
            }
        }

        private void readConfigFile()
        {
            String returnValue = ChooseFile();
            if (returnValue != null)
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.Load(returnValue);
                Iteration(xml_doc.DocumentElement.ChildNodes);
            }
        }


        private void createClasses()
        {
            readConfigFile();
        }

        private void listbox3Check()
        {
            var items = screenTypes
            .Where(item => item.Value.Item1 == null)
            .ToList();

            listBox3.Items.Clear();
            foreach (var item in items)
            {
                listBox3.Items.Add(item.Key);
            }
        }

        private void dependantStateGenereator(String str, STATE state){
            switch (Map(str))
            {
                case ('F'):
                    var type = Utility.Types
                    .Where(type_ => type_.Name.Equals("STATE_" + "W"))
                    .FirstOrDefault();

                    var instance = Activator.CreateInstance(
                        type
                    );

                    Utility.STATES.Add((STATE)instance);

                    screenTypes.Remove(str);
                    screenTypes.Add(str, Tuple.Create<STATE, STATE>(state, (STATE)instance));

                    screenTypes.Add(str + "/W", Tuple.Create<STATE, STATE>((STATE)instance, null));


                    break;
                default:
                    return;
            }
        }

        Dictionary<String, Tuple<STATE, STATE>> screenTypes = new Dictionary<string, Tuple<STATE, STATE>>();
        private void Form1_Load(object sender, EventArgs e)
        {
            createClasses();

            screenTypes.Add("صفحه اول", Tuple.Create<STATE, STATE>(null, null));
            screenTypes.Add("صفحه ورود رمز", Tuple.Create<STATE, STATE>(null, null));
            screenTypes.Add("صفحه ورود مبلغ", Tuple.Create<STATE, STATE>(null, null));
            screenTypes.Add("صفحه لطفا منتظر بمانید", Tuple.Create<STATE, STATE>(null, null));
            screenTypes.Add("صفحه timeout", Tuple.Create<STATE, STATE>(null, null));
            screenTypes.Add("صفحه منوی اصلی", Tuple.Create<STATE, STATE>(null, null));
            //var aa = Enum.GetValues(typeof(Int32)).OfType<int>().Where(i => i.value="" );
            //()listBox3.SelectedItem
            //foreach(var a in aa){
            //}
            listbox3Check();

            //string nspace = "howto_load_treeview_from_xml.STATES";

            //var q = from t in Assembly.GetExecutingAssembly().GetTypes()
            //        where t.IsClass && t.Namespace == nspace
            //        select t;
            //List<Type> list = new List<Type>();
            //foreach (var item in q.ToList())
            //{

            //    //list.Add(item.Name);
            //    list.Add(item);
            //}
            //
            textBox1.Visible = false;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            String returnValue = ChooseFolder();
            if (returnValue != null)
            {
                object oreturnValue = returnValue;
                new Thread(
                    o =>
                    {
                        makeScreenFolder(oreturnValue);
                    }
                ).Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null && listBox2.SelectedItem != null && ifDirectoryExists())
                new_state_creation_handler(listBox1.SelectedItem.ToString(), listBox2.SelectedItem.ToString());
        }

        private void TabPage2_Click(object sender, EventArgs e)
        {
            LoadSTATES();
        }

        public static String numberNormalizer(String arg)
        {
            if (arg == null)
                return "000";
            while (arg.Length < 3)
                arg = "0" + arg;
            return arg;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            XML_Maker.xmlMaker_Handler();
            System.Windows.Forms.MessageBox.Show("Saved doc as test.xml");
        }

        private void LoadTreeViewFromXmlFile(string filename, TreeView trv)
        {
            XmlDocument xml_doc = new XmlDocument();
            xml_doc.Load(filename);
            Utility.xmlDocument = xml_doc;
            trv.Nodes.Clear();
            AddTreeViewChildNodes(trv.Nodes, xml_doc.DocumentElement);
        }

        private void AddTreeViewChildNodes(TreeNodeCollection parent_nodes, XmlNode xml_node)
        {
            foreach (XmlNode child_node in xml_node.ChildNodes)
            {
                TreeNode new_node = parent_nodes.Add(child_node.Name);
                new_node.ContextMenuStrip = contextMenuStrip1;
                AddTreeViewChildNodes(new_node.Nodes, child_node);
                if (new_node.Nodes.Count == 0)
                {
                    TreeNode treenode = new TreeNode(child_node.InnerText);
                    if (new_node.Parent != null)
                    {
                        new_node.Parent.Nodes.Add(treenode);
                        new_node.Remove();
                    }
                }
            }
        }

        private void LoadYourListBox()
        {
            this.listBox1.SelectedIndexChanged -= new EventHandler(ListBox1_SelectedIndexChanged);
            listBox1.DataSource = Utility.Types.ToList();
            listBox1.DisplayMember = nameof(Type.Name);
            this.listBox1.SelectedIndexChanged += new EventHandler(ListBox1_SelectedIndexChanged);
        }

        private void LoadSTATES()
        {
            LoadYourListBox();
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Utility.currentState = null;
            textBox1.Visible = true;
            Utility.dictionary.Clear();
            new_state_creation_handler(listBox1.SelectedItem.ToString(), null);
            //String text;
            //String minedStateType = mineStateType(listBox1.SelectedItem.ToString());
            var properties = Utility.currentInstanceType.GetProperties();

            //switch (minedStateType)
            //{
            //    case ("STATES.STATE_A"):
            //        text = File.ReadAllText("STATE_A.txt");
            //        break;
            //    case ("STATES.STATE_B"):
            //        text = File.ReadAllText("STATE_B.txt");
            //        break;
            //    case ("STATES.STATE_D"):
            //        text = File.ReadAllText("STATE_D.txt");
            //        break;
            //    case ("STATES.STATE_F"):
            //        text = File.ReadAllText("STATE_F.txt");
            //        break;
            //    case ("STATES.STATE_H"):
            //        text = File.ReadAllText("STATE_H.txt");
            //        break;
            //    case ("STATES.STATE_I"):
            //        text = File.ReadAllText("STATE_I.txt");
            //        break;
            //    case ("STATES.STATE_J"):
            //        text = File.ReadAllText("STATE_J.txt");
            //        break;
            //    case ("STATES.STATE_K"):
            //        text = File.ReadAllText("STATE_K.txt");
            //        break;
            //    case ("STATES.STATE_W"):
            //        text = File.ReadAllText("STATE_W.txt");
            //        break;
            //    case ("STATES.STATE_X"):
            //        text = File.ReadAllText("STATE_X.txt");
            //        break;
            //    case ("STATES.STATE_Y"):
            //        text = File.ReadAllText("STATE_Y.txt");
            //        break;
            //    default:
            //        return;
            //}
            //String[] strthis;
            //strthis = text.Split(',');
            listBox2.Items.Clear();
            foreach (var item in properties)
            {
                listBox2.Items.Add(item.Name);
            }
        }
        //

        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        public static String numberNeatter(String givenNumber)
        {
            String returnValue = givenNumber.TrimStart(new Char[] { '0' });
            if (returnValue == "")
                return "0";
            else
                return returnValue;
        }

        public static String removeWord(String str, List<String> words)
        {
            foreach (var word in words)
                if (str.Contains(word))
                    str = str.Replace(word, "");
            return str;
        }

        public static String wordNeater(String toBeNeated, String ext)
        {
            return removeWord(toBeNeated, new List<string>() { "SCREENS\\", ext });
        }

        public static String alternateSuggesterForState()
        {
            bool bl;
            for (int i = 0; i < 1000; ++i)
            {
                bl = false;
                foreach (var stateNumber in localList)
                {
                    if (numberNeatter(stateNumber) == i.ToString())
                    {
                        bl = true;
                        break;
                    }
                }
                if (!bl)
                    return i.ToString();
            }
            return "";
        }

        public static String alternateSuggester()
        {
            bool bl;
            for (int i = 0; i < 1000; ++i)
            {
                bl = false;
                foreach (var file in Directory.GetFiles("SCREENS"))
                {
                    if (file.ToString().Contains(".jpg") && numberNeatter(wordNeater(file.ToString(), ".jpg")) == i.ToString())
                    {
                        bl = true;
                        break;
                    }
                    else if (file.ToString().Contains(".gif") && numberNeatter(wordNeater(file.ToString(), ".gif")) == i.ToString())
                    {
                        bl = true;
                        break;
                    }
                }
                if (!bl)
                    return i.ToString();
            }
            return "";
        }

        private static List<String> localList = new List<String>();
        private static void mineStateNumbersFromXMLFile(XmlNode node)
        {
            if (node.Name == "STATE_NUMBER")
                localList.Add(node.InnerText);

            if(node.HasChildNodes)
                foreach (var xmlNode in node.ChildNodes)
                {
                    mineStateNumbersFromXMLFile((XmlNode)xmlNode);
                }
        }

        public static Tuple<String, bool> CheckStateNumber(String givenNumber)
        {
            String response = alternateSuggesterForState();

            String result = localList.FirstOrDefault(
            item => numberNeatter(item) == givenNumber
            );
            if (result != null)
                return Tuple.Create(response, true);
            else
                return Tuple.Create("", false);
        }


        public static Tuple<String, bool> CheckScreenNumber(String givenNumber)
        {
            String response = alternateSuggester();

            foreach (var file in Directory.GetFiles("SCREENS"))
            {
                if (file.ToString().Contains(".jpg") && numberNeatter(wordNeater(file.ToString(), ".jpg")) == givenNumber)
                        return Tuple.Create(response, true);
                else if (file.ToString().Contains(".gif") && numberNeatter(wordNeater(file.ToString(), ".gif")) == givenNumber)
                        return Tuple.Create(response, true);
            }
            if (response == "")
                return Tuple.Create("", true);
            return Tuple.Create("",false);
        }

        public static String mineStateType(String state)
        {
            String[] str = state.Split('.');
            return str[1] + "." + str[2];
        }

        private Type searchInGeneratedTypes(String arg) {
            Type returnType = null;
            foreach (var item in Utility.Types)
            {
                if (item.Name == arg)
                    returnType = item;
            }
            return returnType;
        }

        private void currentStateSet(String arg)
        {
            //const string objectToInstantiate = "SampleProject.Domain.MyNewTestClass, MyTestProject";
            //var objectType = Type.GetType(objectToInstantiate);
            Utility.currentInstanceType = searchInGeneratedTypes(arg);
            //Utility.currentInstanceType = Type.GetType(arg.ToString());
            //dynamic instantiatedObject = Activator.CreateInstance(objectType);
            Utility.currentStateDynamic = Activator.CreateInstance(Utility.currentInstanceType);
            //switch (arg)
            //{
            //    case ("STATES.STATE_A"):
            //        Utility.currentState = new STATES.STATE_A();
            //        break;
            //    case ("STATES.STATE_B"):
            //        Utility.currentState = new STATES.STATE_B();
            //        break;
            //    case ("STATES.STATE_D"):
            //        Utility.currentState = new STATES.STATE_D();
            //        break;
            //    case ("STATES.STATE_F"):
            //        Utility.currentState = new STATES.STATE_F();
            //        break;
            //    case ("STATES.STATE_H"):
            //        Utility.currentState = new STATES.STATE_H();
            //        break;
            //    case ("STATES.STATE_I"):
            //        Utility.currentState = new STATES.STATE_I();
            //        break;
            //    case ("STATES.STATE_J"):
            //        Utility.currentState = new STATES.STATE_J();
            //        break;
            //    case ("STATES.STATE_K"):
            //        Utility.currentState = new STATES.STATE_K();
            //        break;
            //    case ("STATES.STATE_W"):
            //        Utility.currentState = new STATES.STATE_W();
            //        break;
            //    case ("STATES.STATE_X"):
            //        Utility.currentState = new STATES.STATE_X();
            //        break;
            //    case ("STATES.STATE_Y"):
            //        Utility.currentState = new STATES.STATE_Y();
            //        break;
            //    default:
            //        return;
            //}
        }

        private void new_state_creation_handler(String desiredStateType, String property)
        {
            //desiredStateType = mineStateType(desiredStateType);
            if (property == null)
            {
                currentStateSet(desiredStateType);
            }
            else
            {
                STATES_SWITCHES.SWITCH_ON_STATE_TYPE(desiredStateType, property);
            }
        }

        private bool ifDirectoryExists()
        {
            string root = "SCREENS";
            if (Directory.Exists(root))
                return true;
            return false;
        }
        public String ChooseFile()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }
            return null;
        }

        public String ChooseFolder()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                return folderBrowserDialog1.SelectedPath;
            }
            return null;
        }

        void Copy(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);

            foreach (var file in Directory.GetFiles(sourceDir))
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

            foreach (var directory in Directory.GetDirectories(sourceDir))
                Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }

        void makeScreenFolder(object ogivenPath)
        {
            String givenPath = (String)ogivenPath;
            if (!Directory.Exists("SCREENS"))
            {
                Directory.CreateDirectory("SCREENS");
                Copy(givenPath, "SCREENS");
            }
        }

        private void listBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Focus();
            foreach (var item in Utility.dictionary)
            {
                if (item.Key == listBox2.SelectedItem.ToString())
                {
                    label1.Text = "Selected Item Corresponding Value: " + item.Value;
                    return;
                }
            }
            label1.Text = "Selected Item Corresponding Value:";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String returnValue = ChooseFile();
            if (returnValue != null)
            {
                //string filename = Application.StartupPath;
                //filename = System.IO.Path.Combine(filename, "..\\..");
                //filename = Path.GetFullPath(filename) + "\\test.xml";
                LoadTreeViewFromXmlFile(returnValue, trvItems);
                mineStateNumbersFromXMLFile(Utility.xmlDocument.DocumentElement);
            }
        }

        private bool ifExist(ref String picName)
        {
            foreach (var file in Directory.GetFiles("SCREENS"))
            {
                if (file.ToString().Contains(".jpg") && wordNeater(file.ToString(), ".jpg") == picName)
                {
                    picName += ".jpg";
                    return true;

                }
                else if (file.ToString().Contains(".gif") && wordNeater(file.ToString(), ".gif") == picName)
                {
                    picName += ".gif";
                    return true;
                }
            }
            return false;
        }

        private XmlNode FindXMLNodebyName(String nodeName, XmlNodeList list)
        {
            if (list.Count > 0)
            {
                foreach (XmlNode node in list)
                {
                    if (node.Name.Equals(nodeName)) return node;
                    if (node.HasChildNodes)
                    {
                        XmlNode nodeFound = FindXMLNodebyName(nodeName, node.ChildNodes);
                        if (nodeFound != null)
                            return nodeFound;
                    }
                }
            }
            return null;
        }

        private XmlNode searchForPropertyInXmlDocbyName(XmlNodeList xmlNodeList)
        {
            foreach (XmlNode item in xmlNodeList)
            {
                if (item.Name == Utility.mainForm.trvItems.SelectedNode.Text)
                    return item;
            }
            return null;
        }

        private String inputNormalizer(String str)
        {
            while (str.Length < 3)
                str.Insert(0, "0");
            return str;
        }

        private void renameXmlNode(XmlNode xmlNode, String valueToBeSet)
        {
            XmlNode newXmlNode = Utility.xmlDocument.CreateElement(xmlNode.Name.First() + inputNormalizer(valueToBeSet));
            while(xmlNode.HasChildNodes)
                newXmlNode.AppendChild(xmlNode.ChildNodes[0]);
            xmlNode.ParentNode.AppendChild(newXmlNode);
            xmlNode.ParentNode.RemoveChild(xmlNode);
        }

        private void editExistingState(String valueToBeSet)
        {
            String currentSTATENUMBER = Utility.mainForm.trvItems.SelectedNode.Parent.Text;
            XmlNode foundXmlNode = FindXMLNodebyName(currentSTATENUMBER, Utility.xmlDocument.DocumentElement.ChildNodes);
            XmlNode propertyNode = searchForPropertyInXmlDocbyName(foundXmlNode.ChildNodes);

            Tuple<String, bool> returnedValues;
            propertyNode.InnerText = "";

            switch (Utility.mainForm.trvItems.SelectedNode.Text)
            {
                case ("SCREEN_NUMBER"):
                    returnedValues = CheckScreenNumber(valueToBeSet);
                    if (returnedValues.Item2)
                        propertyNode.InnerText = returnedValues.Item1;
                    break;
                case ("STATE_NUMBER"):
                    returnedValues = CheckStateNumber(valueToBeSet);
                    if (returnedValues.Item2)
                    {
                        propertyNode.InnerText = returnedValues.Item1;
                        renameXmlNode(foundXmlNode, returnedValues.Item1);
                    } else
                        renameXmlNode(foundXmlNode, valueToBeSet);
                    break;
            }
            if (propertyNode.InnerText == "")
                propertyNode.InnerText = valueToBeSet;
            System.Windows.Forms.MessageBox.Show(propertyNode.InnerText);

            string filename = Application.StartupPath;
            filename = System.IO.Path.Combine(filename, "..\\..");
            filename = Path.GetFullPath(filename) + "\\test.xml";
            Utility.xmlDocument.Save(filename);
            LoadTreeViewFromXmlFile(filename, Utility.mainForm.trvItems);
        }

        private void OptionForm()
        { 
            using (Form form = new Form())
            {
                form.StartPosition = FormStartPosition.CenterScreen;
                Button button = new Button
                {
                    Text = "Apply"
                };
                TextBox textBox = new TextBox();

                textBox.Location = new Point(0, 0);
                button.Location = new Point(0, 25);

                button.Click += (s, e) => { editExistingState(textBox.Text); form.Close(); };

                form.Controls.Add(textBox);
                form.Controls.Add(button);


                form.ShowDialog();
            }
        }

        private void trvItems_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
        }

        private void trvItems_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                // Select the clicked node
                trvItems.SelectedNode = trvItems.GetNodeAt(e.X, e.Y);

                if (trvItems.SelectedNode != null)
                {
                    contextMenuStrip1.Show(trvItems, e.Location);
                }
            }
        }

        private Nullable<char> Map(String str)
        {
            switch (str)
            {
                case ("صفحه اول"):
                    return 'A';

                case ("صفحه ورود رمز"):
                    return 'F';
            }
            return null;
        }

        private void Graph()
        {
        System.Windows.Forms.Form form = new System.Windows.Forms.Form();
        //create a viewer object 
        Microsoft.Msagl.GraphViewerGdi.GViewer viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
        //create a graph object 
        Microsoft.Msagl.Drawing.Graph graph = new Microsoft.Msagl.Drawing.Graph("graph");

        var items = screenTypes
        .Where(screenType => screenType.Value.Item2 != null)
        .ToList();

            foreach (var item in items)
            {
                var vr = screenTypes
                .Where(screenType => screenType.Value.Item1 == item.Value.Item2)
                .FirstOrDefault();
                graph.AddEdge(item.Key.ToString(), vr.Key.ToString());
            }

        //create the graph content
        //graph.AddEdge("A", "B");
        //graph.AddEdge("B", "C");
        //graph.AddEdge("A", "C").Attr.Color = Microsoft.Msagl.Drawing.Color.Green;
        //graph.FindNode("A").Attr.FillColor = Microsoft.Msagl.Drawing.Color.Magenta;
        //graph.FindNode("B").Attr.FillColor = Microsoft.Msagl.Drawing.Color.MistyRose;
        //Microsoft.Msagl.Drawing.Node c = graph.FindNode("C");
        //c.Attr.FillColor = Microsoft.Msagl.Drawing.Color.PaleGreen;
        //c.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Diamond;

    //bind the graph to the viewer 
        viewer.Graph = graph;
    //associate the viewer with the form 
        form.SuspendLayout();
        viewer.Dock = System.Windows.Forms.DockStyle.Fill;
        form.Controls.Add(viewer);
        form.ResumeLayout();
        //show the form 
        form.ShowDialog();
        }

        private void relation(String str)
        {
            switch (str)
            {
                case ("صفحه ورود رمز"):
                    var item = screenTypes
                    .Where(screenType => screenType.Key.Equals(str))
                    .FirstOrDefault();

                    var item_2 = screenTypes
                    .Where(screenType => screenType.Key.Equals("صفحه اول"))
                    .FirstOrDefault();

                    STATE state = (STATE)item.Value.Item1;
                    screenTypes.Remove(item_2.Key);
                    screenTypes.Add("صفحه اول", Tuple.Create<STATE, STATE>(item_2.Value.Item1, state));

                    dependantStateGenereator(str, (STATE)item.Value.Item1);
                    break;
            }
        }

        private void func(String str, String fileName){
            var type = Utility.Types
            .Where(type_ => type_.Name.Equals("STATE_" + Map(str).ToString()))
            .FirstOrDefault();

            var instance = Activator.CreateInstance(
                type
            );

            PropertyInfo prop = type.GetProperty("SCREEN_NUMBER", BindingFlags.Public | BindingFlags.Instance);
            prop.SetValue(instance,
                fileName.Replace(Path.GetExtension(fileName), ""),
                null
            );

            Utility.STATES.Add((STATE)instance);

            var item = screenTypes
                .Where(screenType => screenType.Key.Equals(str))
                .FirstOrDefault();

            screenTypes.Remove(item.Key);
            screenTypes.Add(str, Tuple.Create<STATE, STATE>((STATE)instance, null));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            String fileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = Path.GetFileName(openFileDialog1.FileName);
            }

            func(listBox3.SelectedItem.ToString(), fileName);
            //var type = Utility.Types
            //    .Where(type_ => type_.Name.Equals("STATE_A"))
            //    .FirstOrDefault();

            //var instance = Activator.CreateInstance(
            //    type
            //);

            //PropertyInfo prop = type.GetProperty("SCREEN_NUMBER", BindingFlags.Public | BindingFlags.Instance);
            //prop.SetValue(instance,
            //    fileName.Replace(Path.GetExtension(fileName), ""),
            //    null
            //);

            //Utility.STATES.Add((STATE)instance);

            //var item = screenTypes
            //    .Where(screenType => screenType.Key.Equals("صفحه اول"))
            //    .FirstOrDefault();

            //screenTypes.Remove(item.Key);
            //screenTypes.Add("صفحه اول", (STATE)instance);
            relation(listBox3.SelectedItem.ToString());
            Graph();
            listbox3Check();
        }

        private void VIEW_CURRENT_STATES_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}
