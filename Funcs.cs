﻿using System.Drawing;
using System.Windows.Forms;

namespace howto_load_treeview_from_xml
{
    public static class Funcs
    {
        public static void showBitMap(Bitmap img)
        {
            using (Form form = new Form())
            {
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Size = img.Size;

                PictureBox pb = new PictureBox();
                pb.Dock = DockStyle.Fill;
                pb.Image = img;

                form.Controls.Add(pb);
                form.ShowDialog();
            }
        }
    }
}
