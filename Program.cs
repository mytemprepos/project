﻿using System;
using System.Windows.Forms;

namespace howto_load_treeview_from_xml
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Utility.mainForm = new Form1();
            Application.Run(Utility.mainForm);
        }
    }
}
