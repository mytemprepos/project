﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace howto_load_treeview_from_xml
{
    class STATES_SWITCHES
    {
        static String param;
        static Tuple<String, bool> returnedValues;

        //private static bool crtOrEdit()
        //{
        //    DialogResult dialogResult = MessageBox.Show("Choose", "Yes:Create new state, No:Edit existing state.", MessageBoxButtons.YesNo);
        //    if (dialogResult == DialogResult.Yes)
        //        return true;
        //    else
        //        return false;
        //}

        public static void SWITCH_ON_STATE_TYPE(String desiredStateType, String property)
        {
            //if(Utility.currentStateDynamic == null)
            //{
            //    editExistingState(property);
            //    return;
            //}

            param = Utility.mainForm.textBox1.Text;
            PropertyInfo prop = Utility.currentInstanceType.GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                switch (property)
                {
                    case ("SCREEN_NUMBER"):
                        returnedValues = Form1.CheckScreenNumber(param);
                        if (returnedValues.Item2)
                            Utility.currentStateDynamic.SCREEN_NUMBER = returnedValues.Item1;
                        else
                            Utility.currentStateDynamic.SCREEN_NUMBER = param;
                        System.Windows.Forms.MessageBox.Show(Utility.currentStateDynamic.SCREEN_NUMBER);
                        break;
                    case ("STATE_NUMBER"):
                        returnedValues = Form1.CheckStateNumber(param);
                        if (returnedValues.Item2)
                        {
                            //if (crtOrEdit())
                            //    Utility.currentStateDynamic.SCREEN_NUMBER = returnedValues.Item1;
                            //else
                            //{
                            //    Utility.currentStateDynamic = null;
                            //    Utility.currentStateNumber = Utility.mainForm.listBox1.SelectedItem.ToString().Last() + inputNormalizer(Utility.mainForm.textBox1.Text);
                            //    SWITCH_ON_STATE_TYPE(desiredStateType, property);
                            //    return;
                            //}
                        }
                        else
                            Utility.currentStateDynamic.STATE_NUMBER = param;
                        System.Windows.Forms.MessageBox.Show(Utility.currentStateDynamic.STATE_NUMBER);
                        break;
                    default:
                        prop.SetValue(Utility.currentStateDynamic, param, null);
                        break;
                }
                Utility.dictionary.Add(property, Utility.mainForm.textBox1.Text);
            }
            //switch (desiredStateType)
            //{
            //    case ("STATES.STATE_A"):
            //        STATE_A_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_B"):
            //        STATE_B_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_D"):
            //        STATE_D_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_F"):
            //        STATE_F_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_H"):
            //        STATE_H_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_I"):
            //        STATE_I_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_J"):
            //        STATE_J_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_K"):
            //        STATE_K_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_W"):
            //        STATE_W_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_X"):
            //        STATE_X_SWITCH(property);
            //        break;
            //    case ("STATES.STATE_Y"):
            //        STATE_Y_SWITCH(property);
            //        break;
            //    default:
            //        return;
            //}
        }
        //private static void STATE_A_SWITCH(String property)
        //{
        //    STATES.STATE_A mySTATEA = Utility.currentState as STATES.STATE_A;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("GOOD_READ_NEXT_STATE"):
        //            mySTATEA.GOOD_READ_NEXT_STATE = param;
        //            break;
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEA.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEA.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEA.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEA.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEA.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEA.STATE_NUMBER);
        //            break;
        //        case ("READ_CONDITION_1"):
        //            mySTATEA.READ_CONDITION_1 = param;
        //            break;
        //        case ("READ_CONDITION_2"):
        //            mySTATEA.READ_CONDITION_2 = param;
        //            break;
        //        case ("READ_CONDITION_3"):
        //            mySTATEA.READ_CONDITION_3 = param;
        //            break;
        //        case ("CARD_RETURN_FLAG"):
        //            mySTATEA.CARD_RETURN_FLAG = param;
        //            break;
        //        case ("NO_FIT_MATCH_NEXT_STATE"):
        //            mySTATEA.NO_FIT_MATCH_NEXT_STATE = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEA;
        //}
        ////
        //private static void STATE_B_SWITCH(String property)
        //{
        //    STATES.STATE_B mySTATEB = Utility.currentState as STATES.STATE_B;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEB.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEB.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEB.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEB.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEB.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEB.STATE_NUMBER);
        //            break;
        //        case ("TIMEOUT_NEXT_STATE"):
        //            mySTATEB.TIMEOUT_NEXT_STATE = param;
        //            break;
        //        case ("CANCEL_NEXT_STATE"):
        //            mySTATEB.CANCEL_NEXT_STATE = param;
        //            break;
        //        case ("LOCAL_PIN_GOOD_PIN_NEXT_STATE"):
        //            mySTATEB.LOCAL_PIN_GOOD_PIN_NEXT_STATE = param;
        //            break;
        //        case ("LOCAL_PIN_MAX_BAD_NEXT_STATE"):
        //            mySTATEB.LOCAL_PIN_MAX_BAD_NEXT_STATE = param;
        //            break;
        //        case ("LOCAL_PIN_ERROR_SCREEN_NUMBER"):
        //            mySTATEB.LOCAL_PIN_ERROR_SCREEN_NUMBER = param;
        //            break;
        //        case ("REM_PIN_CHECK_STATE"):
        //            mySTATEB.REM_PIN_CHECK_STATE = param;
        //            break;
        //        case ("LOCAL_PIN_MAX_RETRIES"):
        //            mySTATEB.LOCAL_PIN_MAX_RETRIES = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEB;
        //}
        ////
        //private static void STATE_D_SWITCH(String property)
        //{
        //    STATES.STATE_D mySTATED = Utility.currentState as STATES.STATE_D;;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATED.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATED.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATED.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATED.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATED.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATED.STATE_NUMBER);
        //            break;
        //        case ("NEXT_STATE"):
        //            mySTATED.NEXT_STATE = param;
        //            break;
        //        case ("CLEAR_MASK"):
        //            mySTATED.CLEAR_MASK = param;
        //            break;
        //        case ("PRESET_MASK_A"):
        //            mySTATED.PRESET_MASK_A = param;
        //            break;
        //        case ("PRESET_MASK_B"):
        //            mySTATED.PRESET_MASK_B = param;
        //            break;
        //        case ("PRESET_MASK_C"):
        //            mySTATED.PRESET_MASK_C = param;
        //            break;
        //        case ("PRESET_MASK_D"):
        //            mySTATED.PRESET_MASK_D = param;
        //            break;
        //        case ("RESERVED"):
        //            mySTATED.RESERVED = param;
        //            break;
        //        case ("EXTENSION_STATE"):
        //            mySTATED.EXTENSION_STATE = param;
        //            break;
        //        case ("PRESET_MASK_F"):
        //            mySTATED.PRESET_MASK_F = param;
        //            break;
        //        case ("PRESET_MASK_G"):
        //            mySTATED.PRESET_MASK_G = param;
        //            break;
        //        case ("PRESET_MASK_H"):
        //            mySTATED.PRESET_MASK_H = param;
        //            break;
        //        case ("PRESET_MASK_I"):
        //            mySTATED.PRESET_MASK_I = param;
        //            break;
        //        case ("RESERVED_2"):
        //            mySTATED.RESERVED_2 = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATED;
        //}
        //private static void STATE_F_SWITCH(String property)
        //{
        //    STATES.STATE_F mySTATEF = Utility.currentState as STATES.STATE_F;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEF.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEF.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEF.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEF.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEF.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEF.STATE_NUMBER);
        //            break;
        //        case ("TIMEOUT_NEXT_STATE"):
        //            mySTATEF.TIMEOUT_NEXT_STATE = param;
        //            break;
        //        case ("CANCEL_NEXT_STATE"):
        //            mySTATEF.CANCEL_NEXT_STATE = param;
        //            break;
        //        case ("FDK_A_I_NEXT_STAT"):
        //            mySTATEF.FDK_A_I_NEXT_STAT = param;
        //            break;
        //        case ("FDK_B_H_NEXT_STATE"):
        //            mySTATEF.FDK_B_H_NEXT_STATE = param;
        //            break;
        //        case ("FDK_C_G_NEXT_STATE"):
        //            mySTATEF.FDK_C_G_NEXT_STATE = param;
        //            break;
        //        case ("FDK_D_F_NEXT_STATE"):
        //            mySTATEF.FDK_D_F_NEXT_STATE = param;
        //            break;
        //        case ("BUFFER_DISPLAY_SCREEN"):
        //            mySTATEF.BUFFER_DISPLAY_SCREEN = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEF;
        //}
        //private static void STATE_H_SWITCH(String property)
        //{
        //    STATES.STATE_H mySTATEH = Utility.currentState as STATES.STATE_H;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEH.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEH.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEH.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEH.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEH.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEH.STATE_NUMBER);
        //            break;
        //        case ("TIMEOUT_NEXT_STATE"):
        //            mySTATEH.TIMEOUT_NEXT_STATE = param;
        //            break;
        //        case ("CANCEL_NEXT_STATE"):
        //            mySTATEH.CANCEL_NEXT_STATE = param;
        //            break;
        //        case ("FDK_A_I_NEXT_STAT"):
        //            mySTATEH.FDK_A_I_NEXT_STAT = param;
        //            break;
        //        case ("FDK_B_H_NEXT_STATE"):
        //            mySTATEH.FDK_B_H_NEXT_STATE = param;
        //            break;
        //        case ("FDK_C_G_NEXT_STATE"):
        //            mySTATEH.FDK_C_G_NEXT_STATE = param;
        //            break;
        //        case ("FDK_D_F_NEXT_STATE"):
        //            mySTATEH.FDK_D_F_NEXT_STATE = param;
        //            break;
        //        case ("BUFFER_AND_DISPLAY_PARAM"):
        //            mySTATEH.BUFFER_AND_DISPLAY_PARAM = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEH;
        //}
        //private static void STATE_I_SWITCH(String property)
        //{
        //    STATES.STATE_I mySTATEI = Utility.currentState as STATES.STATE_I;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEI.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEI.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEI.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEI.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEI.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEI.STATE_NUMBER);
        //            break;
        //        case ("CENTRAL_RESP_TIMEOUT_NEXT"):
        //            mySTATEI.CENTRAL_RESP_TIMEOUT_NEXT = param;
        //            break;
        //        case ("SEND_TRACK2_DATA"):
        //            mySTATEI.SEND_TRACK2_DATA = param;
        //            break;
        //        case ("SEND_TRACK1_TRACK3_DATA"):
        //            mySTATEI.SEND_TRACK1_TRACK3_DATA = param;
        //            break;
        //        case ("SEND_OPERATION_CODE_BUFFER"):
        //            mySTATEI.SEND_OPERATION_CODE_BUFFER = param;
        //            break;
        //        case ("SEND_AMOUNT_DATA"):
        //            mySTATEI.SEND_AMOUNT_DATA = param;
        //            break;
        //        case ("SEND_PIN_BUFFER_DATA_A"):
        //            mySTATEI.SEND_PIN_BUFFER_DATA_A = param;
        //            break;
        //        case ("SEND_GENERAL_PURPOSE_B_C"):
        //            mySTATEI.SEND_GENERAL_PURPOSE_B_C = param;
        //            break;
        //        case ("IF_EXTENSION"):
        //            mySTATEI.IF_EXTENSION = param;
        //            break;
        //        case ("EXTENSION_STATE_NUMBER"):
        //            mySTATEI.EXTENSION_STATE_NUMBER = param;
        //            break;
        //        case ("EX_SEND_GENERAL_PURPOSE_B_C"):
        //            mySTATEI.EX_SEND_GENERAL_PURPOSE_B_C = param;
        //            break;
        //        case ("SEND_OPTIONAL_FIELD_A_H"):
        //            mySTATEI.SEND_OPTIONAL_FIELD_A_H = param;
        //            break;
        //        case ("SEND_OPTIONAL_FIELD_I_L"):
        //            mySTATEI.SEND_OPTIONAL_FIELD_I_L = param;
        //            break;
        //        case ("SEND_OPTIONAL_FIELD_Q_V"):
        //            mySTATEI.SEND_OPTIONAL_FIELD_Q_V = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEI;
        //}
        //private static void STATE_J_SWITCH(String property)
        //{
        //    STATES.STATE_J mySTATEJ = Utility.currentState as STATES.STATE_J;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("STATE_NUMBER"):
        //            var returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEJ.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEJ.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEJ.STATE_NUMBER);
        //            break;
        //        case ("RECEIPT_DELIVRED_SCREEN"):
        //            mySTATEJ.RECEIPT_DELIVRED_SCREEN = param;
        //            break;
        //        case ("NEXT_STATE"):
        //            mySTATEJ.NEXT_STATE = param;
        //            break;
        //        case ("NO_RECEIPT_DELIVRED_SCREEN"):
        //            mySTATEJ.NO_RECEIPT_DELIVRED_SCREEN = param;
        //            break;
        //        case ("CARD_RETAINED_SCREEN"):
        //            mySTATEJ.CARD_RETAINED_SCREEN = param;
        //            break;
        //        case ("STATEMENT_DELIVRED_SCREEN"):
        //            mySTATEJ.STATEMENT_DELIVRED_SCREEN = param;
        //            break;
        //        case ("BNA_NOTE_RETURN_SCREEN"):
        //            mySTATEJ.BNA_NOTE_RETURN_SCREEN = param;
        //            break;
        //        case ("EXT_STATE_NUMBER"):
        //            mySTATEJ.EXT_STATE_NUMBER = param;
        //            break;
        //        case ("IF_EXTENSION"):
        //            mySTATEJ.IF_EXTENSION = param;
        //            break;
        //        case ("CMP_TAKE_SCREEN"):
        //            mySTATEJ.CMP_TAKE_SCREEN = param;
        //            break;
        //        case ("CMP_DOC_RETURN_FLAG"):
        //            mySTATEJ.CMP_DOC_RETURN_FLAG = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEJ;
        //}
        //private static void STATE_K_SWITCH(String property)
        //{
        //    STATES.STATE_K mySTATEK = Utility.currentState as STATES.STATE_K;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("STATE_NUMBER"):
        //            var returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEK.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEK.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEK.STATE_NUMBER);
        //            break;
        //        case ("NEXT_STATE_INDEX_0"):
        //            mySTATEK.NEXT_STATE_INDEX_0 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_1"):
        //            mySTATEK.NEXT_STATE_INDEX_1 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_2"):
        //            mySTATEK.NEXT_STATE_INDEX_2 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_3"):
        //            mySTATEK.NEXT_STATE_INDEX_3 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_4"):
        //            mySTATEK.NEXT_STATE_INDEX_4 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_5"):
        //            mySTATEK.NEXT_STATE_INDEX_5 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_6"):
        //            mySTATEK.NEXT_STATE_INDEX_6 = param;
        //            break;
        //        case ("NEXT_STATE_INDEX_7"):
        //            mySTATEK.NEXT_STATE_INDEX_7 = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEK;
        //}
        //private static void STATE_W_SWITCH(String property)
        //{
        //    STATES.STATE_W mySTATEW = Utility.currentState as STATES.STATE_W;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("STATE_NUMBER"):
        //            var returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEW.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEW.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEW.STATE_NUMBER);
        //            break;
        //        case ("FDK_A_NEXT_STATE"):
        //            mySTATEW.FDK_A_NEXT_STATE = param;
        //            break;
        //        case ("FDK_B_NEXT_STATE"):
        //            mySTATEW.FDK_B_NEXT_STATE = param;
        //            break;
        //        case ("FDK_C_NEXT_STATE"):
        //            mySTATEW.FDK_C_NEXT_STATE = param;
        //            break;
        //        case ("FDK_D_NEXT_STATE"):
        //            mySTATEW.FDK_D_NEXT_STATE = param;
        //            break;
        //        case ("FDK_F_NEXT_STATE"):
        //            mySTATEW.FDK_F_NEXT_STATE = param;
        //            break;
        //        case ("FDK_G_NEXT_STATE"):
        //            mySTATEW.FDK_G_NEXT_STATE = param;
        //            break;
        //        case ("FDK_H_NEXT_STATE"):
        //            mySTATEW.FDK_H_NEXT_STATE = param;
        //            break;
        //        case ("FDK_I_NEXT_STATE"):
        //            mySTATEW.FDK_I_NEXT_STATE = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEW;
        //}
        //private static void STATE_X_SWITCH(String property)
        //{
        //    STATES.STATE_X mySTATEX = Utility.currentState as STATES.STATE_X;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEX.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEX.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEX.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEX.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEX.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEX.STATE_NUMBER);
        //            break;
        //        case ("TIMEOUT_NEXT_STATE"):
        //            mySTATEX.TIMEOUT_NEXT_STATE = param;
        //            break;
        //        case ("CANCEL_NEXT_STATE"):
        //            mySTATEX.CANCEL_NEXT_STATE = param;
        //            break;
        //        case ("FDK_NEXT_STATE"):
        //            mySTATEX.FDK_NEXT_STATE = param;
        //            break;
        //        case ("EXTENSION_STATE"):
        //            mySTATEX.EXTENSION_STATE = param;
        //            break;
        //        case ("BUFFER_ID"):
        //            mySTATEX.BUFFER_ID = param;
        //            break;
        //        case ("FDK_ACTIVE_MASK"):
        //            mySTATEX.FDK_ACTIVE_MASK = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_A"):
        //            mySTATEX.VALUE_STORED_IF_FDK_A = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_B"):
        //            mySTATEX.VALUE_STORED_IF_FDK_B = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_C"):
        //            mySTATEX.VALUE_STORED_IF_FDK_C = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_D"):
        //            mySTATEX.VALUE_STORED_IF_FDK_D = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_F"):
        //            mySTATEX.VALUE_STORED_IF_FDK_F = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_G"):
        //            mySTATEX.VALUE_STORED_IF_FDK_G = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_H"):
        //            mySTATEX.VALUE_STORED_IF_FDK_H = param;
        //            break;
        //        case ("VALUE_STORED_IF_FDK_I"):
        //            mySTATEX.VALUE_STORED_IF_FDK_I = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEX;
        //}
        //private static void STATE_Y_SWITCH(String property)
        //{
        //    STATES.STATE_Y mySTATEY = Utility.currentState as STATES.STATE_Y;
        //    param = Utility.mainForm.textBox1.Text;
        //    switch (property)
        //    {
        //        case ("SCREEN_NUMBER"):
        //            returnedValues = Form1.CheckScreenNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEY.SCREEN_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEY.SCREEN_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEY.SCREEN_NUMBER);
        //            break;
        //        case ("STATE_NUMBER"):
        //            returnedValues = Form1.CheckStateNumber(param);
        //            if (returnedValues.Item2)
        //                mySTATEY.STATE_NUMBER = returnedValues.Item1;
        //            else
        //                mySTATEY.STATE_NUMBER = param;
        //            System.Windows.Forms.MessageBox.Show(mySTATEY.STATE_NUMBER);
        //            break;
        //        case ("TIMEOUT_NEXT_STATE"):
        //            mySTATEY.TIMEOUT_NEXT_STATE = param;
        //            break;
        //        case ("CANCEL_NEXT_STATE"):
        //            mySTATEY.CANCEL_NEXT_STATE = param;
        //            break;
        //        case ("FDK_NEXT_STATE"):
        //            mySTATEY.FDK_NEXT_STATE = param;
        //            break;
        //        case ("EXTENSION_STATE"):
        //            mySTATEY.EXTENSION_STATE = param;
        //            break;
        //        case ("BUFFER_POSITION"):
        //            mySTATEY.BUFFER_POSITION = param;
        //            break;
        //        case ("FDK_ACTIVE_MASK"):
        //            mySTATEY.FDK_ACTIVE_MASK = param;
        //            break;
        //        case ("MULTI_LANG_EXT_STATE"):
        //            mySTATEY.MULTI_LANG_EXT_STATE = param;
        //            break;
        //        case ("IF_EXTENSION"):
        //            mySTATEY.IF_EXTENSION = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_A"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_A = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_B"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_B = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_C"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_C = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_D"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_D = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_F"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_F = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_G"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_G = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_H"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_H = param;
        //            break;
        //        case ("CD_OPERATION_STORED_IF_FDK_I"):
        //            mySTATEY.CD_OPERATION_STORED_IF_FDK_I = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_A"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_A = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_B"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_B = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_C"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_C = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_D"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_D = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_F"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_F = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_G"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_G = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_H"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_H = param;
        //            break;
        //        case ("SCREEN_BASE_STORED_IF_FDK_I"):
        //            mySTATEY.SCREEN_BASE_STORED_IF_FDK_H = param;
        //            break;
        //        default:
        //            return;
        //    }
        //    Utility.dictionary.Add(property, param);
        //    Utility.currentState = mySTATEY;
        //}
    }
}
