﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace howto_load_treeview_from_xml
{
    public static class Utility
    {
        public static XmlDocument xmlDocument { get; set; }

        public static Dictionary<String, String> dictionary = new Dictionary<String, String>();

        public static Form1 mainForm { get; set; }

        public static object currentState;
        public static dynamic currentStateDynamic;
        public static Type currentInstanceType;

        public static List<String> reflectedStatesList { get; set; }
            = new List<String>();

        public static List<STATE> STATES { get; set; }
            = new List<STATE>();

        public static List<Type> Types { get; set; }
            = new List<Type>();

        //public static String currentStateNumber { get; set; }
    }
}
