﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace howto_load_treeview_from_xml
{
    class XML_Maker
    {
        private static XmlElement el;
        private static XmlNode parent, elem;
        public static void xmlMaker_Handler()
        {
            string filename = Application.StartupPath;
            filename = System.IO.Path.Combine(filename, "..\\..");
            filename = Path.GetFullPath(filename) + "\\test.xml";
            XmlNode xmlNode = Utility.xmlDocument.DocumentElement;
            //String neated = Form1.mineStateType(Utility.mainForm.listBox1.SelectedItem.ToString()).Replace("STATES.", "");
            String neated = Utility.mainForm.listBox1.SelectedItem.ToString();
            el = (XmlElement)Utility.xmlDocument.SelectSingleNode("NDC-STATE-DATA/" + neated);
            if (el != null)
            {
                parent = Utility.xmlDocument.CreateElement(neated.Last() + Form1.numberNormalizer(Utility.currentStateDynamic.STATE_NUMBER));
                el.AppendChild(parent);
                foreach (var item in Utility.currentInstanceType.GetProperties())
                {
                    elem = Utility.xmlDocument.CreateElement(item.Name);
                    elem.InnerText = Form1.numberNormalizer(item.GetValue(Utility.currentStateDynamic, null));
                    parent.AppendChild(elem);
                }
            }
            //switch (neated.Last())
            //{
            //    case ('A'):
            //        xmlMaker_STATE_A();
            //        break;
            //    case ('B'):
            //        xmlMaker_STATE_B();
            //        break;
            //    case ('D'):
            //        xmlMaker_STATE_D();
            //        break;
            //    case ('F'):
            //        xmlMaker_STATE_F();
            //        break;
            //    case ('H'):
            //        xmlMaker_STATE_H();
            //        break;
            //    case ('I'):
            //        xmlMaker_STATE_I();
            //        break;
            //    case ('J'):
            //        xmlMaker_STATE_J();
            //        break;
            //    case ('K'):
            //        xmlMaker_STATE_K();
            //        break;
            //    case ('W'):
            //        xmlMaker_STATE_W();
            //        break;
            //    case ('X'):
            //        xmlMaker_STATE_X();
            //        break;
            //    case ('Y'):
            //        xmlMaker_STATE_Y();
            //        break;
            //    default:
            //        return;
            //}
            el.AppendChild(parent);
            Utility.xmlDocument.Save(filename);
        }

        //private static void xmlMaker_STATE_A()
        //{


        //    STATES.STATE_A iSTATE_A = (STATES.STATE_A)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("A" + Form1.numberNormalizer(iSTATE_A.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_A.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_A.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("GOOD_READ_NEXT_STATE");
        //    elem.InnerText = iSTATE_A.GOOD_READ_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("READ_CONDITION_1");
        //    elem.InnerText = iSTATE_A.READ_CONDITION_1;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("READ_CONDITION_2");
        //    elem.InnerText = iSTATE_A.READ_CONDITION_2;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("READ_CONDITION_3");
        //    elem.InnerText = iSTATE_A.READ_CONDITION_3;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CARD_RETURN_FLAG");
        //    elem.InnerText = iSTATE_A.CARD_RETURN_FLAG;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NO_FIT_MATCH_NEXT_STATE");
        //    elem.InnerText = iSTATE_A.NO_FIT_MATCH_NEXT_STATE;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_B()
        //{
        //    STATES.STATE_B iSTATE_B = (STATES.STATE_B)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("B" + Form1.numberNormalizer(iSTATE_B.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_B.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_B.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("TIMEOUT_NEXT_STATE");
        //    elem.InnerText = iSTATE_B.TIMEOUT_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CANCEL_NEXT_STATE");
        //    elem.InnerText = iSTATE_B.CANCEL_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("LOCAL_PIN_GOOD_PIN_NEXT_STATE");
        //    elem.InnerText = iSTATE_B.LOCAL_PIN_GOOD_PIN_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("LOCAL_PIN_MAX_BAD_NEXT_STATE");
        //    elem.InnerText = iSTATE_B.LOCAL_PIN_MAX_BAD_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("LOCAL_PIN_ERROR_SCREEN_NUMBER");
        //    elem.InnerText = iSTATE_B.LOCAL_PIN_ERROR_SCREEN_NUMBER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("REM_PIN_CHECK_STATE");
        //    elem.InnerText = iSTATE_B.REM_PIN_CHECK_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("LOCAL_PIN_MAX_RETRIES");
        //    elem.InnerText = iSTATE_B.LOCAL_PIN_MAX_RETRIES;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_D()
        //{
        //    STATES.STATE_D iSTATE_D = (STATES.STATE_D)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("D" + Form1.numberNormalizer(iSTATE_D.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_D.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_D.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("RESERVED");
        //    elem.InnerText = iSTATE_D.RESERVED;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("RESERVED_2");
        //    elem.InnerText = iSTATE_D.RESERVED_2;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("LOCALNEXT_STATE_PIN_GOOD_PIN_NEXT_STATE");
        //    elem.InnerText = iSTATE_D.NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CLEAR_MASK");
        //    elem.InnerText = iSTATE_D.CLEAR_MASK;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_A");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_A;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_B");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_B;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_C");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_D");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_D;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_F");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_G");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_H");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("PRESET_MASK_I");
        //    elem.InnerText = iSTATE_D.PRESET_MASK_I;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_F()
        //{
        //    STATES.STATE_F iSTATE_F = (STATES.STATE_F)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("F" + Form1.numberNormalizer(iSTATE_F.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_F.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_F.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("TIMEOUT_NEXT_STATE");
        //    elem.InnerText = iSTATE_F.TIMEOUT_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CANCEL_NEXT_STATE");
        //    elem.InnerText = iSTATE_F.CANCEL_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_A_I_NEXT_STAT");
        //    elem.InnerText = iSTATE_F.FDK_A_I_NEXT_STAT;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_B_H_NEXT_STATE");
        //    elem.InnerText = iSTATE_F.FDK_B_H_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_C_G_NEXT_STATE");
        //    elem.InnerText = iSTATE_F.FDK_C_G_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_D_F_NEXT_STATE");
        //    elem.InnerText = iSTATE_F.FDK_D_F_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("BUFFER_DISPLAY_SCREEN");
        //    elem.InnerText = iSTATE_F.BUFFER_DISPLAY_SCREEN;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_H()
        //{
        //    STATES.STATE_H iSTATE_H = (STATES.STATE_H)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("H" + Form1.numberNormalizer(iSTATE_H.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_H.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_H.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("TIMEOUT_NEXT_STATE");
        //    elem.InnerText = iSTATE_H.TIMEOUT_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CANCEL_NEXT_STATE");
        //    elem.InnerText = iSTATE_H.CANCEL_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_A_I_NEXT_STAT");
        //    elem.InnerText = iSTATE_H.FDK_A_I_NEXT_STAT;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_B_H_NEXT_STATE");
        //    elem.InnerText = iSTATE_H.FDK_B_H_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_C_G_NEXT_STATE");
        //    elem.InnerText = iSTATE_H.FDK_C_G_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_D_F_NEXT_STATE");
        //    elem.InnerText = iSTATE_H.FDK_D_F_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("BUFFER_AND_DISPLAY_PARAM");
        //    elem.InnerText = iSTATE_H.BUFFER_AND_DISPLAY_PARAM;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_I()
        //{
        //    STATES.STATE_I iSTATE_I = (STATES.STATE_I)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("I" + Form1.numberNormalizer(iSTATE_I.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_I.SCREEN_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_I.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CENTRAL_RESP_TIMEOUT_NEXT");
        //    elem.InnerText = iSTATE_I.CENTRAL_RESP_TIMEOUT_NEXT;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_TRACK2_DATA");
        //    elem.InnerText = iSTATE_I.SEND_TRACK2_DATA;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_TRACK1_TRACK3_DATA");
        //    elem.InnerText = iSTATE_I.SEND_TRACK1_TRACK3_DATA;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_OPERATION_CODE_BUFFER");
        //    elem.InnerText = iSTATE_I.SEND_OPERATION_CODE_BUFFER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_AMOUNT_DATA");
        //    elem.InnerText = iSTATE_I.SEND_AMOUNT_DATA;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_PIN_BUFFER_DATA_A");
        //    elem.InnerText = iSTATE_I.SEND_PIN_BUFFER_DATA_A;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_GENERAL_PURPOSE_B_C");
        //    elem.InnerText = iSTATE_I.SEND_GENERAL_PURPOSE_B_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("IF_EXTENSION");
        //    elem.InnerText = iSTATE_I.SEND_GENERAL_PURPOSE_B_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("IF_EXTENSION");
        //    elem.InnerText = iSTATE_I.SEND_GENERAL_PURPOSE_B_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("EXTENSION_STATE_NUMBER");
        //    elem.InnerText = iSTATE_I.EXTENSION_STATE_NUMBER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_OPTIONAL_FIELD_A_H");
        //    elem.InnerText = iSTATE_I.EX_SEND_GENERAL_PURPOSE_B_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("EX_SEND_GENERAL_PURPOSE_B_C");
        //    elem.InnerText = iSTATE_I.EX_SEND_GENERAL_PURPOSE_B_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_OPTIONAL_FIELD_I_L");
        //    elem.InnerText = iSTATE_I.SEND_OPTIONAL_FIELD_I_L;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SEND_OPTIONAL_FIELD_Q_V");
        //    elem.InnerText = iSTATE_I.SEND_OPTIONAL_FIELD_Q_V;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_J()
        //{
        //    STATES.STATE_J iSTATE_J = (STATES.STATE_J)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("J" + Form1.numberNormalizer(iSTATE_J.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_J.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("RECEIPT_DELIVRED_SCREEN");
        //    elem.InnerText = iSTATE_J.RECEIPT_DELIVRED_SCREEN;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE");
        //    elem.InnerText = iSTATE_J.NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NO_RECEIPT_DELIVRED_SCREEN");
        //    elem.InnerText = iSTATE_J.NO_RECEIPT_DELIVRED_SCREEN;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CARD_RETAINED_SCREEN");
        //    elem.InnerText = iSTATE_J.CARD_RETAINED_SCREEN;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("STATEMENT_DELIVRED_SCREEN");
        //    elem.InnerText = iSTATE_J.STATEMENT_DELIVRED_SCREEN;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("BNA_NOTE_RETURN_SCREEN");
        //    elem.InnerText = iSTATE_J.BNA_NOTE_RETURN_SCREEN;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("EXT_STATE_NUMBER");
        //    elem.InnerText = iSTATE_J.EXT_STATE_NUMBER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("IF_EXTENSION");
        //    elem.InnerText = iSTATE_J.IF_EXTENSION;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CMP_TAKE_SCREEN");
        //    elem.InnerText = iSTATE_J.CMP_TAKE_SCREEN;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_K()
        //{
        //    STATES.STATE_K iSTATE_K = (STATES.STATE_K)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("K" + Form1.numberNormalizer(iSTATE_K.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_K.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_0");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_0;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_1");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_1;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_2");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_2;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_3");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_3;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_4");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_4;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_5");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_5;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_6");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_6;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("NEXT_STATE_INDEX_7");
        //    elem.InnerText = iSTATE_K.NEXT_STATE_INDEX_7;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_W()
        //{
        //    STATES.STATE_W iSTATE_W = (STATES.STATE_W)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("W" + Form1.numberNormalizer(iSTATE_W.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_W.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_A_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_A_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_B_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_B_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_C_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_C_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_D_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_D_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_F_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_F_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_G_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_G_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_H_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_H_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_I_NEXT_STATE");
        //    elem.InnerText = iSTATE_W.FDK_I_NEXT_STATE;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_X()
        //{
        //    STATES.STATE_X iSTATE_X = (STATES.STATE_X)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("X" + Form1.numberNormalizer(iSTATE_X.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_X.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = iSTATE_X.SCREEN_NUMBER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("TIMEOUT_NEXT_STATE");
        //    elem.InnerText = iSTATE_X.TIMEOUT_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CANCEL_NEXT_STATE");
        //    elem.InnerText = iSTATE_X.CANCEL_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_NEXT_STATE");
        //    elem.InnerText = iSTATE_X.FDK_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("EXTENSION_STATE");
        //    elem.InnerText = iSTATE_X.EXTENSION_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("BUFFER_ID");
        //    elem.InnerText = iSTATE_X.BUFFER_ID;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_ACTIVE_MASK");
        //    elem.InnerText = iSTATE_X.FDK_ACTIVE_MASK;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_A");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_A;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_B");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_B;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_C");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_D");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_D;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_F");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_G");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_G;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_H");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_H;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("VALUE_STORED_IF_FDK_I");
        //    elem.InnerText = iSTATE_X.VALUE_STORED_IF_FDK_I;
        //    parent.AppendChild(elem);
        //}

        //private static void xmlMaker_STATE_Y()
        //{
        //    STATES.STATE_Y iSTATE_Y = (STATES.STATE_Y)Utility.currentState;
        //    parent = Utility.xmlDocument.CreateElement("Y" + Form1.numberNormalizer(iSTATE_Y.STATE_NUMBER));
        //    el.AppendChild(parent);

        //    elem = Utility.xmlDocument.CreateElement("STATE_NUMBER");
        //    elem.InnerText = Form1.numberNormalizer(iSTATE_Y.STATE_NUMBER);
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_NUMBER");
        //    elem.InnerText = iSTATE_Y.SCREEN_NUMBER;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("TIMEOUT_NEXT_STATE");
        //    elem.InnerText = iSTATE_Y.TIMEOUT_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CANCEL_NEXT_STATE");
        //    elem.InnerText = iSTATE_Y.CANCEL_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_NEXT_STATE");
        //    elem.InnerText = iSTATE_Y.FDK_NEXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("EXTENSION_STATE");
        //    elem.InnerText = iSTATE_Y.EXTENSION_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("BUFFER_POSITION");
        //    elem.InnerText = iSTATE_Y.BUFFER_POSITION;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("FDK_ACTIVE_MASK");
        //    elem.InnerText = iSTATE_Y.FDK_ACTIVE_MASK;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("MULTI_LANG_EXT_STATE");
        //    elem.InnerText = iSTATE_Y.MULTI_LANG_EXT_STATE;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("IF_EXTENSION");
        //    elem.InnerText = iSTATE_Y.IF_EXTENSION;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_A");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_A;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_B");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_B;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_C");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_D");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_D;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_F");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_G");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_G;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_H");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_H;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("CD_OPERATION_STORED_IF_FDK_I");
        //    elem.InnerText = iSTATE_Y.CD_OPERATION_STORED_IF_FDK_I;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_A");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_A;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_B");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_B;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_C");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_C;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_D");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_D;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_F");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_F;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_G");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_G;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_H");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_H;
        //    parent.AppendChild(elem);

        //    elem = Utility.xmlDocument.CreateElement("SCREEN_BASE_STORED_IF_FDK_I");
        //    elem.InnerText = iSTATE_Y.SCREEN_BASE_STORED_IF_FDK_I;
        //    parent.AppendChild(elem);
        //}
    }
}
